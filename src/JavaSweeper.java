import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import sweeper.Box;
import sweeper.Coord;
import sweeper.Game;
import sweeper.Ranges;

public class JavaSweeper extends JFrame {

    private final Game game;

   private final int COLS = 9;
   private final int ROWS = 9;
   private final int bombs = 10;
    private final int IMAGE_SIZE = 50;


    private JPanel panel;
    private JLabel label;
    public static void main(String[] args) {
        new JavaSweeper();
    }

    private  JavaSweeper (){
        game = new Game(COLS,ROWS,bombs);
        game.start();
        setImages();
        initPanel();
        initLabel();
        initFrame();

    }

    private void initLabel() {
        label = new JLabel(getMessage());
        Font font = new Font("Tahoma", Font.BOLD,18);
        label.setFont(font);
        add (label, BorderLayout.SOUTH);
    }

    private void initPanel() {
        panel = new JPanel(){
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                for (Coord coord : Ranges.getAllCoords())
                    g.drawImage((Image)game.getBox(coord).image,
                            coord.x * IMAGE_SIZE,
                            coord.y * IMAGE_SIZE, this);




            }
        };

       panel.addMouseListener(new MouseAdapter() {
           @Override
           public void mousePressed(MouseEvent e) {
               int x = e.getX() / IMAGE_SIZE;
               int y = e.getY() / IMAGE_SIZE;
               Coord coord = new Coord(x, y);
               switch (e.getButton()){
                   case MouseEvent.BUTTON1 : game.pressLeftButton(coord);
                   break;
                   case MouseEvent.BUTTON3 : game.pressRightButton(coord);
                   break;
                   case MouseEvent.BUTTON2 : game.start();
                   break;

               }
               label.setText(getMessage());
               panel.repaint();

           }
       });

        panel.setPreferredSize(new Dimension(
                Ranges.getSize().x * IMAGE_SIZE,
                Ranges.getSize().y * IMAGE_SIZE));
        add(panel);
    }

    private void initFrame() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("Java Sweeper");
        setVisible(true);
        setResizable(false);
        pack();
        setLocationRelativeTo(null);
    }

    private Image getImage(String name){
        String filename = "img/" + name + ".png";
        ImageIcon icon = new ImageIcon(getClass().getResource(filename));
        return icon.getImage();
    }

    private void setImages(){
        for(Box box : Box.values())
            box.image = getImage(box.name().toLowerCase());
        setIconImage(getImage("icon"));

    }

    private  String getMessage (){
        switch (game.getState()) {
            case BOMBED : return "Boooom, aha-ha-ha! You Lose!";
            case WINNER : return  "Congratulations! All bombs have been marked";
             case PLAYED :
            default :  if (game.getTotalFlagged() == 0)
                return "Welcome!";
            else
                return  "Are you sure? Flagged " +
                        game.getTotalFlagged() + " of "
                        + game.getTotalBombs() + " bombs";

        }
    }

}
