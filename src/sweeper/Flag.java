package sweeper;

class Flag {
    private Matrix flagMap;
    private  int totalFlageed;
    private  int totalClosed;

    void start(){
        flagMap = new Matrix(Box.CLOSED);
        totalFlageed = 0;
        totalClosed = Ranges.getSquare();

    }

    Box get (Coord coord){
        return  flagMap.get(coord);
    }

    void setOpenedToBox(Coord cord) {

        flagMap.set(cord, Box.OPENED);
        totalClosed--;
    }

    private void setFlagedToBox(Coord coord) {
        flagMap.set(coord, Box.FLAGGED);
        totalFlageed++;
    }

    void toggleFlagedToBox(Coord coord) {
        switch (flagMap.get(coord)){
            case FLAGGED : setClosedToBox(coord);
            break;
            case CLOSED : setFlagedToBox(coord);
            break;
        }

    }

    private void setClosedToBox(Coord coord) {
        flagMap.set(coord, Box.CLOSED);
        totalFlageed--;
    }

    int getTotalFlageed() {
        return totalFlageed;
    }

    int getTotalClosed() {
        return totalClosed;
    }

     void setFlaggedToLastClosedBpxes() {
        for(Coord coord : Ranges.getAllCoords())
            if(Box.CLOSED == flagMap.get(coord))
                setFlagedToBox(coord);
    }

     void setBombToBox(Coord coord) {
        flagMap.set(coord, Box.BOMBED);
    }

     void setOpenedToClosedBox(Coord coord) {
        if(Box.CLOSED == flagMap.get(coord)){
            flagMap.set(coord, Box.OPENED);

        }
    }

     void setNobombToFlaggedBox(Coord coord) {
        if(Box.FLAGGED == flagMap.get(coord))
            flagMap.set(coord, Box.NOBOMB);
    }

     int getCountOfFlaggedBoxesAround(Coord coord) {
        int count = 0;
        for (Coord around : Ranges.getCoordsAround(coord))
            if(flagMap.get(around) == Box.FLAGGED)
                count++;
        return count;
    }
}
